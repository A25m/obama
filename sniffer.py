from scapy.all import *

pkts = sniff( filter='tcp[tcpflags] == tcp-syn' , prn=lambda x:x.summary()) # include ask?

#wireshark filter: tcp.flags.syn==1 && tcp.flags.ack==0
#scapy tcp[tcpflags] == tcp-syn and tcp[tcpflags] != tcp-ask
#Модифицировать код сетевого сниффера для отслеживания только TCP SYN пакетов с помощью фильтров.
#Фреймворк Scapy использует синтаксис Berkley Packet Filter (BPF).
#Попробуй поискать информацию на тему “BPF TCP Ack”.
#Фильтр должен выглядеть так tcp[tcpflags] == ???
